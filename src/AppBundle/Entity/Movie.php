<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Movie
 *
 * @ORM\Table(name="movie")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MovieRepository")
 */
class Movie
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, unique=true)
     */
    private $title;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="releasedAt", type="datetime")
     */
    private $releasedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="director", type="string", length=255)
     */
    private $director;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="UserLike", mappedBy="movie")
     */
    private $likes;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="UserDislike", mappedBy="movie")
     */
    private $dislikes;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->likes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dislikes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Movie
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set releasedAt
     *
     * @param \DateTime $releasedAt
     *
     * @return Movie
     */
    public function setReleasedAt($releasedAt)
    {
        $this->releasedAt = $releasedAt;

        return $this;
    }

    /**
     * Get releasedAt
     *
     * @return \DateTime
     */
    public function getReleasedAt()
    {
        return $this->releasedAt;
    }

    /**
     * Set director
     *
     * @param string $director
     *
     * @return Movie
     */
    public function setDirector($director)
    {
        $this->director = $director;

        return $this;
    }

    /**
     * Get director
     *
     * @return string
     */
    public function getDirector()
    {
        return $this->director;
    }

    /**
     * Add like
     *
     * @param \AppBundle\Entity\UserLike $like
     *
     * @return Movie
     */
    public function addLike(\AppBundle\Entity\UserLike $like)
    {
        $this->likes[] = $like;

        return $this;
    }

    /**
     * Remove like
     *
     * @param \AppBundle\Entity\UserLike $like
     */
    public function removeLike(\AppBundle\Entity\UserLike $like)
    {
        $this->likes->removeElement($like);
    }

    /**
     * Get likes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLikes()
    {
        return $this->likes;
    }

    /**
     * Add dislike
     *
     * @param \AppBundle\Entity\UserDislike $dislike
     *
     * @return Movie
     */
    public function addDislike(\AppBundle\Entity\UserDislike $dislike)
    {
        $this->dislikes[] = $dislike;

        return $this;
    }

    /**
     * Remove dislike
     *
     * @param \AppBundle\Entity\UserDislike $dislike
     */
    public function removeDislike(\AppBundle\Entity\UserDislike $dislike)
    {
        $this->dislikes->removeElement($dislike);
    }

    /**
     * Get dislikes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDislikes()
    {
        return $this->dislikes;
    }
}
