<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="UserLike", mappedBy="user")
     */
    private $likes;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="UserDislike", mappedBy="user")
     */
    private $dislikes;

    public function __construct()
    {
        parent::__construct();

        $this->likes = new ArrayCollection();
        $this->dislikes = new ArrayCollection();
    }

    /**
     * Add like
     *
     * @param \AppBundle\Entity\UserLike $like
     *
     * @return User
     */
    public function addLike(\AppBundle\Entity\UserLike $like)
    {
        $this->likes[] = $like;

        return $this;
    }

    /**
     * Remove like
     *
     * @param \AppBundle\Entity\UserLike $like
     */
    public function removeLike(\AppBundle\Entity\UserLike $like)
    {
        $this->likes->removeElement($like);
    }

    /**
     * Get likes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLikes()
    {
        return $this->likes;
    }

    /**
     * Add dislike
     *
     * @param \AppBundle\Entity\UserDislike $dislike
     *
     * @return User
     */
    public function addDislike(\AppBundle\Entity\UserDislike $dislike)
    {
        $this->dislikes[] = $dislike;

        return $this;
    }

    /**
     * Remove dislike
     *
     * @param \AppBundle\Entity\UserDislike $dislike
     */
    public function removeDislike(\AppBundle\Entity\UserDislike $dislike)
    {
        $this->dislikes->removeElement($dislike);
    }

    /**
     * Get dislikes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDislikes()
    {
        return $this->dislikes;
    }
}
