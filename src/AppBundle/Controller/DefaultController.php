<?php

namespace AppBundle\Controller;

use AppBundle\Entity\UserDislike;
use AppBundle\Entity\UserLike;
use AppBundle\Form\MovieType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @param Request $request
     * @param $page
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/{page}", defaults={"page": "1"}, requirements={"page"="\d+"}, name="homepage")
     */
    public function indexAction(Request $request, $page)
    {
        $movies = $this->getDoctrine()->getRepository('AppBundle:Movie')->findAll();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($movies, $request->query->getInt('page', $page), 3);

        return $this->render('@App/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @param $appreciationValue
     * @param $movieId
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/appreciation/{appreciationValue}/{movieId}", name="appreciation")
     */
    public function appreciationAction($appreciationValue, $movieId)
    {
        $em = $this->getDoctrine()->getManager();

        $movie = $em->getRepository('AppBundle:Movie')->find($movieId);
        $user = $this->get('security.token_storage')->getToken()->getUser();

        if ($appreciationValue == 'liked') {
            $like = $em->getRepository('AppBundle:UserLike')->findOneByUserAndMovie($user, $movie);
            $dislike = $em->getRepository('AppBundle:UserDislike')->findOneByUserAndMovie($user, $movie);
            if (!$like) {
                $like = new UserLike();
            }
            $like->setMovie($movie);
            $like->setUser($user);

            $em->persist($like);

            if ($dislike) {
                $em->remove($dislike);
            }

            $em->flush();

            return $this->redirectToRoute('homepage');
        }

        $dislike = $em->getRepository('AppBundle:UserDislike')->findOneByUserAndMovie($user, $movie);
        $like = $em->getRepository('AppBundle:UserLike')->findOneByUserAndMovie($user, $movie);
        if (!$dislike) {
            $dislike = new UserDislike();
        }
        $dislike->setMovie($movie);
        $dislike->setUser($user);

        $em->persist($dislike);

        if ($like) {
            $em->remove($like);
        }

        $em->flush();

        return $this->redirectToRoute('homepage');
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     * @Route("/create_movie", name="create_movie")
     */
    public function createMovieAction(Request $request)
    {
        $movieForm = $this->createForm(MovieType::class);

        $movieForm->handleRequest($request);

        if ($movieForm->isSubmitted() && $movieForm->isValid()) {
            $movie = $movieForm->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($movie);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }

        return $this->render('@App/movie.html.twig', [
            'movieForm' => $movieForm->createView(),
        ]);
    }

    /**
     * @Route("/recommendations", name="recommendations")
     */
    public function recommendationsAction()
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $movies = $this->get('recommendations')->getRecommendedMovies($user);

        return $this->render('@App/recommendations.html.twig', [
            'movies' => $movies,
        ]);
    }
}
