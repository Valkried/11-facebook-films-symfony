<?php

namespace AppBundle\Service;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class RecommendationService
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param User $user
     *
     * @return array
     */
    public function getRecommendedMovies(User $user)
    {
        $likes = $this->em->getRepository('AppBundle:UserLike')->findByUser($user);
        $userMovies = [];
        $otherLikes = [];
        $users = [];
        $moviesRaw = [];
        $movies = [];

        foreach ($likes as $like)
        {
            // Films aimés par l'utilisateur
            $userMovies[] = $like->getMovie();

            foreach ($like->getMovie()->getLikes() as $otherLike) {
                if (!in_array($otherLike, $likes)) {
                    // Likes de chaque film aimé, sans le like de l'utilisateur
                    $otherLikes[] = $otherLike;
                }
            }
        }

        foreach ($otherLikes as $otherLike)
        {
            // Utilisateurs ayant aimé les mêmes films
            $users[] = $otherLike->getUser();
        }

        foreach ($users as $user) {
            foreach ($user->getLikes() as $userLike) {
                if (!in_array($userLike->getMovie(), $userMovies)) {
                    $moviesRaw[] = $userLike->getMovie();
                }
            }
        }

        foreach ($moviesRaw as $movie) {
            if (!array_key_exists($movie->getId(), $movies)) {
                $movies[$movie->getId()] = [
                    'id' => $movie->getId(),
                    'count' => 1,
                    'movie' => $movie,
                ];
            } else {
                $movies[$movie->getId()]['count']++;
            }
        }

        usort($movies, function($a, $b) {
            if ($a['count'] == $b['count']) {
                return 0;
            }
            return ($a['count'] < $b['count']) ? 1 : -1;
        });

        return $movies;
    }
}