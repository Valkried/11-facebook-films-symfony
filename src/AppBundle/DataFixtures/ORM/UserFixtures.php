<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $admin = new User();
        $admin->setUsername('Admin');
        $admin->setEmail('admin@example.com');
        $admin->setPlainPassword('admin');
        $admin->setEnabled(true);
        $admin->addRole('ROLE_ADMIN');
        $manager->persist($admin);

        $user1 = new User();
        $user1->setUsername('Jean Noël');
        $user1->setEmail('jeannoel@example.com');
        $user1->setPlainPassword('jeannoel');
        $user1->setEnabled(true);
        $manager->persist($user1);

        $user2 = new User();
        $user2->setUsername('Jean Bobby');
        $user2->setEmail('jeanbobby@example.com');
        $user2->setPlainPassword('jeanbobby');
        $user2->setEnabled(true);
        $manager->persist($user2);

        $user3 = new User();
        $user3->setUsername('Jean Charles');
        $user3->setEmail('jeancharles@example.com');
        $user3->setPlainPassword('jeancharles');
        $user3->setEnabled(true);
        $manager->persist($user3);

        $manager->flush();

        $this->addReference('admin', $admin);
        $this->addReference('user1', $user1);
        $this->addReference('user2', $user2);
        $this->addReference('user3', $user3);
    }
}