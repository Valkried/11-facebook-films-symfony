<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\UserDislike;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class UserDislikeFixtures extends Fixture  implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $dislike1 = new UserDislike();
        $dislike1->setUser($this->getReference('user1'));
        $dislike1->setMovie($this->getReference('boum'));
        $manager->persist($dislike1);

        $dislike2 = new UserDislike();
        $dislike2->setUser($this->getReference('user1'));
        $dislike2->setMovie($this->getReference('terminator3'));
        $manager->persist($dislike2);

        $dislike3 = new UserDislike();
        $dislike3->setUser($this->getReference('user2'));
        $dislike3->setMovie($this->getReference('boum'));
        $manager->persist($dislike3);

        $dislike4 = new UserDislike();
        $dislike4->setUser($this->getReference('user3'));
        $dislike4->setMovie($this->getReference('boum'));
        $manager->persist($dislike4);

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            MovieFixtures::class,
        );
    }
}