<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Movie;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class MovieFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $movie1 = new Movie();
        $movie1->setTitle('Terminator');
        $movie1->setDirector('James Cameron');
        $movie1->setReleasedAt(new \DateTime('1985-04-24'));
        $manager->persist($movie1);

        $movie2 = new Movie();
        $movie2->setTitle('Terminator 2');
        $movie2->setDirector('James Cameron');
        $movie2->setReleasedAt(new \DateTime('1991-10-16'));
        $manager->persist($movie2);

        $movie3 = new Movie();
        $movie3->setTitle('Terminator 3');
        $movie3->setDirector('Jonathan Mostow');
        $movie3->setReleasedAt(new \DateTime('2003-08-06'));
        $manager->persist($movie3);

        $movie4 = new Movie();
        $movie4->setTitle('La Boum');
        $movie4->setDirector('Claude Pinoteau');
        $movie4->setReleasedAt(new \DateTime('1980-12-17'));
        $manager->persist($movie4);

        $movie5 = new Movie();
        $movie5->setTitle('Le Labyrinthe de Pan');
        $movie5->setDirector('Guillermo del Toro');
        $movie5->setReleasedAt(new \DateTime('2006-11-01'));
        $manager->persist($movie5);

        $manager->flush();

        $this->addReference('terminator', $movie1);
        $this->addReference('terminator2', $movie2);
        $this->addReference('terminator3', $movie3);
        $this->addReference('boum', $movie4);
        $this->addReference('labyrinthe', $movie5);
    }
}