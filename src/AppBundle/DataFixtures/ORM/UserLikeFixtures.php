<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\UserLike;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class UserLikeFixtures extends Fixture  implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $like1 = new UserLike();
        $like1->setUser($this->getReference('user1'));
        $like1->setMovie($this->getReference('terminator'));
        $manager->persist($like1);

        $like3 = new UserLike();
        $like3->setUser($this->getReference('user2'));
        $like3->setMovie($this->getReference('terminator2'));
        $manager->persist($like3);

        $like5 = new UserLike();
        $like5->setUser($this->getReference('user3'));
        $like5->setMovie($this->getReference('terminator'));
        $manager->persist($like5);

        $like6 = new UserLike();
        $like6->setUser($this->getReference('user1'));
        $like6->setMovie($this->getReference('terminator3'));
        $manager->persist($like6);

        $like8 = new UserLike();
        $like8->setUser($this->getReference('user2'));
        $like8->setMovie($this->getReference('terminator3'));
        $manager->persist($like8);

        $like9 = new UserLike();
        $like9->setUser($this->getReference('user3'));
        $like9->setMovie($this->getReference('terminator3'));
        $manager->persist($like9);

        $like2 = new UserLike();
        $like2->setUser($this->getReference('user1'));
        $like2->setMovie($this->getReference('labyrinthe'));
        $manager->persist($like2);

        $like7 = new UserLike();
        $like7->setUser($this->getReference('user3'));
        $like7->setMovie($this->getReference('labyrinthe'));
        $manager->persist($like7);

        $like4 = new UserLike();
        $like4->setUser($this->getReference('user2'));
        $like4->setMovie($this->getReference('labyrinthe'));
        $manager->persist($like4);

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            MovieFixtures::class,
        );
    }
}